import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

Meteor.publish('snippets', function (userId) {
  let id = new Mongo.ObjectID();
  this.added('snippets', id, { _id: id, name: 'test 1', description: 'This is a test', code: 'Array' });
  id = new Mongo.ObjectID();
  this.added('snippets', id, { _id: id, name: 'test 2', description: 'This is a test', code: 'Array' });
  id = new Mongo.ObjectID();
  this.added('snippets', id, { _id: id, name: 'test 3', description: 'This is a test', code: 'Array' });
  id = new Mongo.ObjectID();
  this.added('snippets', id, { _id: id, name: 'test 4', description: 'This is a test', code: 'Array' });
  id = new Mongo.ObjectID();
  this.added('snippets', id, { _id: id, name: 'test 5', description: 'This is a test', code: 'Array' });
  this.ready();
});