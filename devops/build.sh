#!/bin/bash
VERSION=$(cat ../package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')
METEOR_SETTINGS='$(cat ../settings.json)'
REGISTRY=vps.wijnen.in:5000

meteor build ../../dist --directory
cp ../ecosystem.config.js ../../dist/bundle/
cp ./dockerfile ../../dist/bundle/
#cp ../settings.json ../../dist/bundle/
#cp ../report ../../dist/bundle -R
echo dockerfile ../../dist/bundle/.dockerignore

cd ../../dist/bundle/
docker build -f ./dockerfile -t sixpaq/snippetbox:${VERSION} --build-arg METEOR_SETTINGS="$METEOR_SETTINGS" .
docker tag sixpaq/snippetbox:${VERSION} ${REGISTRY}/snippetbox:latest
docker push ${REGISTRY}/snippetbox:latest
