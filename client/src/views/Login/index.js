import React from 'react';
import { Fade } from 'react-reveal';
import { Row, Col } from 'react-bootstrap';
import './login.scss';

export default class Login extends React.Component {
  render() {
    return (
      <div className="login-page">
        <Row>
          <div className="col-sm-12 col-md-6 login-box flex-box">
            <div>
              <h1>Snippet Box</h1>
              <h2>Login</h2>
              <form>
                <div>
                  <label className="control-label">Username:</label>
                  <input className="form-control" name="username" />
                </div>
                <div>
                  <label className="control-label">Password:</label>
                  <input className="form-control" name="password" tyoe="password" />
                </div>
                <div className="btn-group btn-group-justified">
                  <div className="btn btn-primary">Login</div>
                </div>
              </form>
            </div>
          </div>
          <div className="col-xs-12 col-md-6 flex-box">
          </div>
        </Row>
      </div>
    );
  }
};