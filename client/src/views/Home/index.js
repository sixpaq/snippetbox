import React from 'react';
import SnippetList from '../../components/SnippetList';

export default Home = (props) => ( 
  <div>
    <header className="content__title">
    <h1>Dashboard</h1>
    <small>Welcome to your collection of coding snippets!</small>
    
    <div className="actions">
        <a href="" className="actions__item zmdi zmdi-trending-up"></a>
        <a href="" className="actions__item zmdi zmdi-check-all"></a>
    
        <div className="dropdown actions__item">
            <i data-toggle="dropdown" className="zmdi zmdi-more-vert"></i>
            <div className="dropdown-menu dropdown-menu-right">
                <a href="" className="dropdown-item">Refresh</a>
                <a href="" className="dropdown-item">Manage Widgets</a>
                <a href="" className="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
    </header>
    
    <div className="row quick-stats">
        <div className="col-sm-6 col-md-3">
            <div className="quick-stats__item">
                <div className="quick-stats__info">
                    <h2>291</h2>
                    <small>Snippets</small>
                </div>
            </div>
        </div>
    
        <div className="col-sm-6 col-md-3">
            <div className="quick-stats__item">
                <div className="quick-stats__info">
                    <h2>12</h2>
                    <small>Followers</small>
                </div>
            </div>
        </div>
    
        <div className="col-sm-6 col-md-3">
          <div className="quick-stats__item">
            <div className="quick-stats__info">
              <h2>3</h2>
              <small>Your score</small>
            </div>
          </div>
        </div>
    
        <div className="col-sm-6 col-md-3">
          <div className="quick-stats__item">
            <div className="quick-stats__info">
              <h2>5</h2>
              <small>Updated snippets</small>
            </div>
          </div>
        </div>
    </div>
    
    
    <div className="row">
      <div className="col-lg-6">
        <div className="card">
          <div className="card-body">
            <h4 className="card-title">Top 5</h4>
            <SnippetList />
          </div>
        </div>
      </div>

      <div className="col-lg-6">
        <div className="card">
          <div className="card-body">
            <h4 className="card-title">Recent comments</h4>
            <h6 className="card-subtitle">.....</h6>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </div>
        </div>
      </div>
    </div>

    <div className="row">
      <div className="col-lg-6">
        <div className="card">
          <div className="card-body">
            <h4 className="card-title">Leaderboard</h4>
            <h6 className="card-subtitle">.....</h6>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </div>
        </div>
      </div>

      <div className="col-lg-6">
        <div className="card">
          <div className="card-body">
            <h4 className="card-title">News</h4>
            <h6 className="card-subtitle">.....</h6>
          </div>
        </div>
      </div>
    </div>
  </div>
);
