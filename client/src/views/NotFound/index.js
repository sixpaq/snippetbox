import React from 'react';

export default NotFound = (props) => ( 
  <div>
    <header className="content__title">
      <h1>404 - Not Found</h1>
    </header>  
  </div>
);
