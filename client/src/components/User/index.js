import React from 'react';
import Gravatar from 'react-gravatar';

export default User = (props) => (
  <div className="user">
    <div className="user__info" data-toggle="dropdown">
      <Gravatar email="sixpaq@gmail.com" size={48} rating="pg" default="monsterid" className="gravatar" default={null} />
      
      <div>
          <div className="user__name">Mike Wijnen</div>
          <div className="user__email">sixpaq@gmail.com</div>
      </div>
    </div>

    <div className="dropdown-menu">
        <a className="dropdown-item" href="">View Profile</a>
        <a className="dropdown-item" href="">Settings</a>
        <a className="dropdown-item" href="">Logout</a>
    </div>
  </div>
);
