import React from 'react';
import { Link } from 'react-router-dom';

import User from '../User';

export default Sidebar = (props) => (
  <aside className="sidebar">
    <div className="scrollbar-inner">
      <User />

      <ul className="navigation">
          <li className="navigation__active"><Link to="/"><i className="zmdi zmdi-home"></i> Home</Link></li>

          <li className="navigation__sub">
              <a href=""><i className="zmdi zmdi-labels"></i> Snippets</a>

              <ul>
                  <li><Link to="/snippets">View all</Link></li>
                  <li><Link to="/snippets/starred">Most stared</Link></li>
                  <li><Link to="/snippets/recent">Recently viewed</Link></li>
              </ul>
          </li>

          <li className="navigation__sub">
              <a href=""><i className="zmdi zmdi-accounts"></i> Followers</a>

              <ul>
                  <li><Link to="/followers">View all</Link></li>
                  <li><Link to="/followers/recent">Recent</Link></li>
              </ul>
          </li>
      </ul>
    </div>
  </aside>
);
