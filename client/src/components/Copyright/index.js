import React from 'react';
import { Link } from 'react-router-dom';

export default Copyright = (props) => {
    const year = new Date().getFullYear();
    return (
        <footer className="footer hidden-xs-down">
            <p>Copyright &copy; {year} Snippet Box. All rights reserved.</p>

            <ul className="nav footer__nav">
                <Link className="nav-link" to="/">Homepage</Link>
                <Link className="nav-link" to="/snippets">Snippets</Link>
                <Link className="nav-link" to="/followers">Support</Link>
                <Link className="nav-link" to="/forum">Forum</Link>
                <Link className="nav-link" to="/contacts">Contacts</Link>
            </ul>
        </footer>
    );
};