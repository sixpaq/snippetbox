import React from 'react';
import { Mongo } from 'meteor/mongo';
import { Row, Col, ListGroup, ListGroupItem, Button } from 'react-bootstrap';
import Modal from 'react-modal';
import { withTracker } from 'meteor/react-meteor-data';
import Editor from '../Editor';
// import Modal from '../ModalScreen';

const Snippets = new Mongo.Collection('snippets');

class SnippetList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false };
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }

  show() {
    this.setState({ showEditor: true });
  }

  hide() {
    this.setState({ showEditor: false });
  }

  render() {
    const showEditor = (this.state || {}).showEditor;
    console.log('showEditor', showEditor);
    return (
      <div className="snippet-list">
        { showEditor ? "yes" : "no" }
        <Modal isOpen={showEditor} className={{base: 'layer-content'}} overlayClassName={{base: 'layer-overlay'}}>
          <Button  onClick={this.hide}>Close</Button>
          <h1>
            Test
          </h1>
          <Editor />
        </Modal>
        <table className="table mb-0">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {(this.props.items || []).map(item => (
            <tr key={item._id}>
              <td><div onClick={this.show}>{item.name}</div></td>
              <td>{item.description}</td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
};

export default withTracker(props => {
  const handle = Meteor.subscribe('snippets');

  return {
    currentUser: Meteor.user(),
    listLoading: !handle.ready(),
    items: Snippets.find().fetch(),
  };
})(SnippetList);