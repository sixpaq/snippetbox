import React from 'react';
import AceEditor from 'react-ace';

import 'brace/mode/javascript';
import 'brace/theme/tomorrow';

export default Editor = (props) => (
  <div>
    <AceEditor
      mode="javascript"
      theme="tomorrow"
      onChange={() => {

      }}
      width="100%"
      name="edit"
      editorProps={{$blockScrolling: true}}
    />
  </div>
);