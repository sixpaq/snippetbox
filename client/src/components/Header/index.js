import React from 'react';
import { Link } from 'react-router-dom';

export default Header = (props) => (
  <header className="header">
    <div className="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
      <i className="zmdi zmdi-menu"></i>
    </div>

    <div className="logo hidden-sm-down">
      <h1><Link to="/">Snippet Box</Link></h1>
    </div>

    <form className="search">
        <div className="search__inner">
            <input type="text" className="search__text" placeholder="Search for snippets..." />
            <i className="zmdi zmdi-search search__helper" data-sa-action="search-close"></i>
        </div>
    </form>

    <ul className="top-nav">
        <li className="hidden-xl-up"><a href="" data-sa-action="search-open"><i className="zmdi zmdi-search"></i></a></li>

        <li className="dropdown">
            <a href="" data-toggle="dropdown" className="top-nav__notify"><i className="zmdi zmdi-email"></i></a>
            <div className="dropdown-menu dropdown-menu-right dropdown-menu--block">
                <div className="dropdown-header">
                    Messages

                    <div className="actions">
                        <a href="messages.html" className="actions__item zmdi zmdi-plus"></a>
                    </div>
                </div>
            </div>
        </li>

        <li className="dropdown top-nav__notifications">
            <a href="" data-toggle="dropdown" className="top-nav__notify">
                <i className="zmdi zmdi-notifications"></i>
            </a>
            <div className="dropdown-menu dropdown-menu-right dropdown-menu--block">
                <div className="dropdown-header">
                    Notifications

                    <div className="actions">
                        <a href="" className="actions__item zmdi zmdi-check-all" data-sa-action="notifications-clear"></a>
                    </div>
                </div>
            </div>
        </li>

        <li className="dropdown hidden-xs-down">
            <a href="" data-toggle="dropdown"><i className="zmdi zmdi-check-circle"></i></a>

            <div className="dropdown-menu dropdown-menu-right dropdown-menu--block" role="menu">
                <div className="dropdown-header">Tasks</div>
            </div>
        </li>

        <li className="dropdown hidden-xs-down">
            <a href="" data-toggle="dropdown"><i className="zmdi zmdi-apps"></i></a>

            <div className="dropdown-menu dropdown-menu-right dropdown-menu--block" role="menu">
                <div className="row app-shortcuts">
                    <a className="col-4 app-shortcuts__item" href="">
                        <i className="zmdi zmdi-calendar"></i>
                        <small className="">Calendar</small>
                    </a>
                    <a className="col-4 app-shortcuts__item" href="">
                        <i className="zmdi zmdi-file-text"></i>
                        <small className="">Files</small>
                    </a>
                    <a className="col-4 app-shortcuts__item" href="">
                        <i className="zmdi zmdi-email"></i>
                        <small className="">Email</small>
                    </a>
                    <a className="col-4 app-shortcuts__item" href="">
                        <i className="zmdi zmdi-trending-up"></i>
                        <small className="">Reports</small>
                    </a>
                    <a className="col-4 app-shortcuts__item" href="">
                        <i className="zmdi zmdi-view-headline"></i>
                        <small className="">News</small>
                    </a>
                    <a className="col-4 app-shortcuts__item" href="">
                        <i className="zmdi zmdi-image"></i>
                        <small className="">Gallery</small>
                    </a>
                </div>
            </div>
        </li>

        <li className="dropdown hidden-xs-down">
            <a href="" data-toggle="dropdown"><i className="zmdi zmdi-more-vert"></i></a>

            <div className="dropdown-menu dropdown-menu-right">
                <a href="" className="dropdown-item" data-sa-action="fullscreen">Fullscreen</a>
                <a href="" className="dropdown-item">Clear Local Storage</a>
                <a href="" className="dropdown-item">Settings</a>
            </div>
        </li>
    </ul>
  </header>
);
