import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Switch } from 'react-router';
import { IntlProvider, addLocaleData } from 'react-intl';
import nl from 'react-intl/locale-data/nl';
import registerServiceWorker from './registerServiceWorker';
import moment from 'moment';
import Modal from 'react-modal';

import Header from './components/Header';
import Loader from './components/Loader';
import Sidebar from './components/Sidebar';
import Copyright from './components/Copyright';

import Home from './views/Home';
import Snippets from './views/Snippets';
import Followers from './views/Followers';
import NotFound from './views/NotFound';
import Login from './views/Login';

import 'jquery';
import 'jquery.scrollbar';
import 'jquery-scroll-lock';
import 'peity';

import '../../public/js/app.min.js';

addLocaleData([...nl]);
moment.locale('nl');

import createHistory from 'history/createBrowserHistory';

const history = createHistory();

Meteor.startup(() => {
  render(
    <IntlProvider locale="nl">
      <Router history={history}>
        <Switch>
          <Route path="/login" component={Login} />
          <Route render={(props) => (
            <main className="main">
              <Loader />
              <Header />
              <Sidebar />

              <section className="content">
                <Switch>
                  <Route path="/" exact component={Home} />
                  <Route path="/snippets" exact component={Snippets} />
                  <Route path="/followers" exact component={Followers} />
                  <Route component={NotFound} />
                </ Switch>

                <Copyright />
              </section>
            </main>
          )} />
        </ Switch>
      </Router>
    </IntlProvider>,
    document.getElementById('root'),
  );

  Modal.setAppElement('#root');

  registerServiceWorker();
});